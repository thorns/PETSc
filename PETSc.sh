#! /bin/sh
#/*@@
#  @file   petsc.sh
#  @date   Fri 29 Aug 2003
#  @author Thomas Radke
#  @desc
#          Setup for an external PETSc installation
#  @enddesc
# @@*/

# /*@@
#   @routine    CCTK_Search
#   @date       Wed Jul 21 11:16:35 1999
#   @author     Tom Goodale
#   @desc
#   Used to search for something in various directories
#   @enddesc
#@@*/

CCTK_Search()
{
  eval  $1=""
  if test $# -lt 4 ; then
    cctk_basedir=""
  else
    cctk_basedir="$4/"
  fi
  for cctk_place in $2
    do
#      echo $ac_n "  Looking in $cctk_place""...$ac_c" #1>&6
      if test -r "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
      if test -d "$cctk_basedir$cctk_place/$3" ; then
#        echo "$ac_t""... Found" #1>&6
        eval $1="$cctk_place"
        break
      fi
#      echo "$ac_t"" No" #1>&6
    done

  return
}


# Check that MPI is there
if [ -z "$MPI" -o "$MPI" = 'none' ]; then
  echo "BEGIN ERROR"
  echo 'Thorn PETSc requires MPI.  Please configure with MPI ' \
       'or remove PETSc from your configuration ThornList.'
  echo "END ERROR"
  exit 2
fi

# Work out PETSc's installation directory
if [ -z "$PETSC_DIR" ]; then
  echo "BEGIN MESSAGE"
  echo 'PETSc selected but no PETSC_DIR set. Checking some places...'
  echo "END MESSAGE"
  CCTK_Search PETSC_DIR '/ /usr /usr/local /usr/local/petsc /usr/local/packages/petsc /usr/local/apps/petsc' include/petsc.h
  if [ -z "$PETSC_DIR" ] ; then
     echo "BEGIN ERROR"
     echo 'Thorn PETSc requires an external installation of the PETSc ' \
          'library.  Please set PETSC_DIR to the directory of this ' \
          'installation, or remove PETSc from your configuration ThornList.'
     echo "END ERROR"
     exit 2
  fi
  echo "BEGIN MESSAGE"
  echo "Found a PETSc package in $PETSC_DIR"
  echo "END MESSAGE"
else
  echo "BEGIN MESSAGE"
  echo "Using PETSc package in $PETSC_DIR"
  echo "END MESSAGE"
fi

# Set platform-specific libraries
if [ -z "$PETSC_ARCH_LIBS" ]; then
  case "$PETSC_ARCH" in
    alpha) PETSC_ARCH_LIBS='dxml' ;;
    IRIX64) PETSC_ARCH_LIBS='fpe blas complib.sgimath' ;;
    linux)  PETSC_ARCH_LIBS='flapack fblas g2c mpich'  ;;
    linux_intel) PETSC_ARCH_LIBS='mkl_lapack mkl_def guide' ;;
    linux-gnu) PETSC_ARCH_LIBS='mkl_lapack mkl_def guide' ;;
    linux64_intel) PETSC_ARCH_LIBS='mkl_lapack mkl guide' ;;
    rs6000_64) PETSC_ARCH_LIBS='essl' ;;
    *) echo 'BEGIN ERROR'
       echo "No PETSc support for architecture '$PETSC_ARCH'."
       echo 'Please set the variable PETSC_ARCH_LIBS manually,'
       echo 'and/or send a request to <cactusmaint@cactuscode.org>.'
       echo 'END ERROR'
       exit 2
  esac
fi

# Set version-specific library directory
# (version 2.3.0 and newer use different library directories)
if [ -e "$PETSC_DIR/lib/$PETSC_ARCH" -o -e "$PETSC_DIR/lib/libpetsc.a" ]; then
  PETSC_LIB_INFIX=''
else
  PETSC_LIB_INFIX='/libO'
fi

# Set version-specific libraries
# (version 2.2.0 and newer do not have libpetscsles.a any more)
if [ -e "$PETSC_DIR/lib$PETSC_LIB_INFIX/$PETSC_ARCH/libpetscksp.a" -o -e "$PETSC_DIR/lib/libpetscksp.a" ]; then
  PETSC_SLES_LIBS="petscksp"
else
  PETSC_SLES_LIBS="petscsles"
fi

# Set the PETSc libs, libdirs and includedirs
PETSC_INC_DIRS='$(PETSC_DIR)/include $(PETSC_DIR)/bmake/$(PETSC_ARCH)'
PETSC_LIB_DIRS='$(PETSC_DIR)/lib'$PETSC_LIB_INFIX'/$(PETSC_ARCH)'
PETSC_LIBS="petscts petscsnes $PETSC_SLES_LIBS petscdm petscmat petscvec petsc $PETSC_ARCH_LIBS"

echo 'BEGIN MAKE_DEFINITION'
echo "PETSC_DIR      = $PETSC_DIR"
echo "PETSC_ARCH     = $PETSC_ARCH"
echo "PETSC_INC_DIRS = $PETSC_INC_DIRS"
echo "PETSC_LIB_DIRS = $PETSC_LIB_DIRS"
echo "PETSC_LIBS     = $PETSC_LIBS"
echo 'END MAKE_DEFINITION'

# Write the data out to the headers and makefiles
echo 'INCLUDE_DIRECTORY $(PETSC_INC_DIRS)'
echo 'LIBRARY_DIRECTORY $(PETSC_LIB_DIRS) $(X_LIB_DIR)'
echo 'LIBRARY $(PETSC_LIBS) X11 $(MPI_LIBS)'
